#include<stdio.h>
#include<conio.h>
#include<windows.h>
#include<time.h>
#include<cstdlib>

#define IZQUIERDA 75
#define DERECHA 77
#define ARRIBA 72

/*
1. elementos pantalla
2. enemigos y logica de puntos
3. agregar menu + colores
*/

struct Bala {
	//Estructura Bala, que almacena las posiciones x e y
	int posX;
	int posY;
};

struct Enemigo{
	//Estructura Enemigo, que almacena las posiciones, el tipo, el estado de eliminado y la recompensa a obtener si se logra eliminar
	int posX;
	int posY;
	int tipo;
	bool eliminado;
	int recompensa;
};

void gotoxy(int x, int y){
	//Identificador de la consola
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);//Tipo control: Salida de la consola
	
	//Estructura COORD de windows.h
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	
	//Seteo de la posici�n del cursor
	SetConsoleCursorPosition(hCon, dwPos);
}

void nave_pintar(int x, int y){
	//Pintar nave en su posicion actualizada
	gotoxy(x,y);	printf("  %c",30);
	gotoxy(x,y+1);	printf(" %c%c%c",40,207,41);
	gotoxy(x,y+2);	printf("%c%c %c%c",30,190,190,30);
}

void nave_borrar(int x, int y){
	//Borrar nave en su ultima posicion
	gotoxy(x,y);	printf("     ");
	gotoxy(x,y+1);	printf("     ");
	gotoxy(x,y+2);	printf("     ");
}

void ocultar_cursor(){
	//Identificador de la consola
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);//Tipo control: Salida de la consola
	
	//Apariencia del cursor
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize = 100;
	cci.bVisible = FALSE;
	
	//Seteo de la posici�n del cursor
	SetConsoleCursorInfo(hCon, &cci);
}

void pintar_limites(){
	//Bordes horizontales
	for(int i=2; i<76;i++){
		gotoxy(i,1);	printf("%c",205);
		gotoxy(i,31);	printf("%c",205);
	}
	
	//Bordes verticales
	for(int i=2; i<31;i++){
		gotoxy(2,i);	printf("%c",186);
		gotoxy(75,i);	printf("%c",186);
	}
	//Esquinas
	gotoxy(2,1);	printf("%c",201);
	gotoxy(2,31);	printf("%c",200);
	gotoxy(75,1);	printf("%c",187);
	gotoxy(75,31);	printf("%c",188);
}

void naves_pintar(int naves){
	//Pintar 04 naves iniciales
	//Si "naves" disminuye de valor, pintara menos naves
	for(int i=0;i<4;i++){
		nave_borrar(3+6*i,28);
	}
	for(int i=0;i<naves;i++){
		nave_pintar(3+6*i,28);
	}
}

void nave_morir(int x, int y, bool tiempo_terminado){
	//Pendiente: Muerte de la nave
}

void pintar_nivel(int nivel){
	//Pintar nivel actual
	gotoxy(4,2);	printf("Nivel: ");
	gotoxy(11,2);	printf("%d", nivel);
}

void pintar_puntos(int puntos){
	//Pintar puntos acumulados
	gotoxy(32,2);	printf("Puntaje:     ");
	gotoxy(41,2);	printf("%04d", puntos);
}

void pintar_tiempo(time_t tiempo_restante){
	//Tiempo restante en segundos
	gotoxy(64,2);	printf("Tiempo:   ");
	gotoxy(72,2);	printf("%02ld",tiempo_restante);
}

void borrar_enemigo(int x, int y){
	//Borrar enemigo (un solo tipo)
	gotoxy(x,y);	printf("     ");
	gotoxy(x,y+1);	printf("     ");
	gotoxy(x,y+2);	printf("     ");
}

void pintar_enemigo(int x, int y){
	//Pintar enemigo (un solo tipo)
	gotoxy(x,y);	printf("%c%c %c%c",30,190,190,30);
	gotoxy(x,y+1);	printf(" %c%c%c",40,207,41);
	gotoxy(x,y+2);	printf("  %c",30);
}

int main(){
	//Inicializa el generador de numeros aleatorios
	srand(time(0));
	//Posiciones de la nave principal
	int x = 36, y = 25;
	int puntos = 0, naves = 4, nivel = 1;
	
	time_t inicio = time(NULL), tiempo_restante, duracion=30;

	struct Bala balas[duracion*100];//100 balas por segundo
	struct Enemigo enemigos[4];
	int disparo = 0;
	bool game_over = false;
	
	pintar_limites();
	ocultar_cursor();
	
	//Inicializar las posiciones de los enemigos
	for(int i=0; i<4; i++){
		enemigos[i].posX = 20+i*6;
		enemigos[i].posY = 6;
		enemigos[i].tipo = 1;
		enemigos[i].eliminado = false;
		enemigos[i].recompensa = 100;
	}
	
	while(!game_over){
		//Pintar cabecera
		pintar_nivel(nivel);
		pintar_puntos(puntos);
		
		//Tiempo en segundos
		tiempo_restante = duracion-(time(NULL)-inicio);
		pintar_tiempo(tiempo_restante);
		
		naves_pintar(naves);
		//Borrado de la nave
		nave_borrar(x,y);
		
		if(kbhit()){
			char tecla = getch();
			//Verificar que la bala de dispare, pero no salga de los limites
			if(tecla == ARRIBA){
				balas[disparo].posX = x+2;
				balas[disparo].posY = y-5;
				disparo++;
			}
			//Verificar que la nave se mueva, pero no salga de los limites
			if(tecla == IZQUIERDA && x>3) x--;
			if(tecla == DERECHA && x+6<76) x++;
		}
		
		//Pintado de la nave
		nave_pintar(x,y);
		
		//Pintar balas en posiciones variantes
		for(int i=0; i<=disparo; i++){
			gotoxy(balas[i].posX,balas[i].posY);	printf(" ");
			//Pintar recorrido de la bala
			if(balas[i].posY>4){
				balas[i].posY--;
				gotoxy(balas[i].posX,balas[i].posY);	printf("*");
			}
			//Verificar si la bala alcanzo a un enemigo
			for(int j=0; j<4; j++){
				if (enemigos[j].eliminado == false){
					if(enemigos[j].posX<=balas[i].posX && balas[i].posX<=enemigos[j].posX+4 && enemigos[j].posY<=balas[i].posY && balas[i].posY<=enemigos[j].posY+2){
						enemigos[j].eliminado = true;
						puntos+=enemigos[j].recompensa;
					}
				}
			}
		}
		
		int horizonal, vertical;
		
		//Generacion de cambios de posicion para los enemigos (vertical y horizonal)
		if (3<enemigos[0].posX && enemigos[3].posX<50){
			horizonal = (rand() % 3)-1;
		}else{
			//Cambio de direccion de movimiento cuando llega a los limites horizontales
			if (enemigos[0].posX<=3){
				horizonal = 1;
			}else{
				horizonal = -1;
			}
		}
		if (4<enemigos[0].posY && enemigos[0].posY<22){
			vertical  = (rand() % 3)-1;
		}else{
			//Cambio de direccion de movimiento cuando llega a los limites verticales
			if (enemigos[0].posY<=4){
				vertical = 1;
			}else{
				vertical = -1;
			}
		}
		
		//Borrar, mover y pintar enemigos
		for(int i=0; i<4; i++){
			//Borrar enemigo
			borrar_enemigo(enemigos[i].posX, enemigos[i].posY);
			//Cambiar posicion
			enemigos[i].posX+=horizonal;
			enemigos[i].posY+=vertical;
			if (enemigos[i].eliminado == false){
				//Pintar enemigo
				pintar_enemigo(enemigos[i].posX, enemigos[i].posY);
			}
		}
		
		//Verificar: punto>400 o tiempo=0
		if(puntos>=400 || tiempo_restante<=0){
			for(int i=0; i<4; i++){
				borrar_enemigo(enemigos[i].posX,enemigos[i].posY);
				enemigos[i].posX = 20+i*6;
				enemigos[i].posY = 6;
				enemigos[i].eliminado = false;
			}
			inicio = time(NULL);

			if(puntos>=400){
				puntos=0;
				nivel++;
			}else{
				puntos=0;
				naves--;
			}
		}
		
		//Ocultar cursor siempre
		ocultar_cursor();
		Sleep(60);//Detiene las iteraciones por milisegundos
	}
	
	return 0;
}
